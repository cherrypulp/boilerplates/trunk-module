/**
 * @see https://stripe.com/docs/testing#cards
 */

/**
 * @param code
 * @return {{creditCardNumber: *, postalCode: string, cvcCode: string, expirationDate: string}}
 */
export const generateValidCard = code => ({
    creditCardNumber: code,
    expirationDate: '1250',
    cvcCode: '123',
    postalCode: '42222',
});

/**
 * @type {{CARD_DEFAULT: {creditCardNumber: *, postalCode: string, cvcCode: string, expirationDate: string}, CARD_3D_SECURE_DECLINED: {creditCardNumber: *, postalCode: string, cvcCode: string, expirationDate: string}, CARD_3D_SECURE: {creditCardNumber: *, postalCode: string, cvcCode: string, expirationDate: string}, CARD_3D_SECURE_2: {creditCardNumber: *, postalCode: string, cvcCode: string, expirationDate: string}}}
 */
export const CreditCards = {
    CARD_DEFAULT: generateValidCard('4242424242424242'),
    CARD_3D_SECURE: generateValidCard('4000000000003063'),
    CARD_3D_SECURE_2: generateValidCard('4000000000003220'),
    CARD_3D_SECURE_DECLINED: generateValidCard('4000008400001629'),
};
