// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

/**
 * Check the presence of a value in window.__app
 */
Cypress.Commands.add('checkWindowApp', (key, value) => {
    cy.window()
        .its(`__app.${key}`)
        .should('equal', value);
});

// Cypress.Commands.add('iframe', (iframeSelector = '', elSelector = 'body') => {
//     return cy.get(`iframe${iframeSelector}`, { timeout: 10000 })
//         .should(($iframe) => {
//             expect($iframe.contents().find(elSelector)).to.exist;
//         })
//         .then(($iframe) => cy.wrap($iframe.contents().find('body')));
// });

Cypress.Commands.overwrite('screenshot', (original, ...args) => {
    if (Cypress.config('allowScreenshots')) {
        original(...args);
    }
});
