// https://docs.cypress.io/guides/guides/plugins-guide.html
const path = require('path');
require('dotenv').config({
    path: path.resolve(`${__dirname}/../../../../../.env`),
});

/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

/**
 * @type {Cypress.PluginConfig}
 */
module.exports = (on, config) => {
    // on('file:preprocessor', webpack({
    //  webpackOptions: require('@vue/cli-service/webpack.config'),
    //  watchOptions: {}
    // }))

    config.baseUrl = process.env.APP_URL;
    config.env = {
        site_url: process.env.APP_URL,
        title: process.env.APP_NAME,
    };

    return Object.assign({}, config);
};
