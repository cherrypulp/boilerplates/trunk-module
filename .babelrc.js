const presets = [
    [
        '@babel/preset-env',
        {
            corejs: 3,
            loose: true,
            modules: 'commonjs',
            useBuiltIns: 'usage',
        },
    ],
];
const plugins = [
    ['@babel/plugin-proposal-decorators', { legacy: true }],
    ['@babel/plugin-proposal-class-properties', { loose: false }],
    ['@babel/plugin-proposal-private-methods', { loose: false }],
    ['@babel/plugin-proposal-nullish-coalescing-operator', { loose: false }],
    ['@babel/plugin-proposal-optional-chaining', { loose: false }],
    '@babel/plugin-transform-object-assign',
    '@babel/plugin-proposal-object-rest-spread',
    '@babel/plugin-proposal-export-default-from',
    '@babel/plugin-proposal-export-namespace-from',
    '@babel/plugin-transform-runtime',
    'wildcard',
    'import-glob',
];

module.exports = {
    presets,
    plugins,
};
