# Trunk Module Name

> Short description


## Installation

Make sure all dependencies have been installed before moving on:

- [Node.js](http://nodejs.org/) >= 8.0.0
- [Trunk](https://www.npmjs.com/package/@cherrypulp/trunk) >= 0.0.7

```console
npm install --save @cherrypulp/trunk-module-name
```


## Quick start

### Configuration and setup

```javascript
// config/trunk-module-name.js
export default {
    mySetting: false,          // explanation of that setting
};
```

```javascript
// main.js
import { TrunkModuleServiceProvider } from '@cherrypulp/trunk-module-name';
...
app.register(new TrunkModuleServiceProvider(app));
...
```


## Documentation

### Something

Explanation about what something does.

```javascript
app.Something('foo');
```




## Versioning

Versioned using [SemVer](http://semver.org/).

## Contribution

Please raise an issue if you find any. Pull requests are welcome!

## Author

  + **Stéphan Zych** - [monkey_monk](https://gitlab.com/monkey_monk)

## License

This project is licensed under the MIT License - see the [LICENSE](https://gitlab.com/cherrypulp/boilerplates/trunk-module/blob/master/LICENSE) file for details.


## TODO

- [ ] do it
