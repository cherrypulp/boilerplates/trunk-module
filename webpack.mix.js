const path = require('path');
const mix = require('laravel-mix');
require('laravel-mix-stylelint');
require('laravel-mix-eslint');

const paths = {
    dist: 'dist',
    src: 'src',
};

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath(paths.dist)

    .eslint({
        cache: process.env.NODE_ENV === 'production',
    })
    .js(`${paths.src}/js/index.js`, 'js')

    .stylelint({
        context: './',
        files: ['**/*.scss'],
        syntax: null,
    })
    .sass(`${paths.src}/scss/index.scss`, 'css')

    .sourceMaps()
    .version();

mix.config.fileLoaderDirs.fonts = `${paths.dist}/fonts`;
mix.config.fileLoaderDirs.images = `${paths.dist}/img`;

mix.options({
    postCss: [
        // @see https://github.com/ismamz/postcss-utilities
        require('postcss-utilities'),
        // @see https://github.com/cssnano/cssnano
        require('cssnano'),
    ],
    processCssUrls: false,
}).webpackConfig({
    context: path.resolve(__dirname, paths.src),
    devtool: 'inline-source-map',
    module: {
        // @see https://www.npmjs.com/package/import-glob-loader
        rules: [
            {
                test: /\.sass$/,
                use: ['import-glob-loader', 'css-loader', 'sass-loader'],
            },
            {
                test: /\.scss$/,
                use: ['import-glob-loader'],
            },
            {
                test: /\.js$/,
                use: ['import-glob-loader'],
            },
        ],
    },
    resolve: {
        alias: {
            '@': path.resolve(__dirname, paths.src),
        },
    },
    target: 'web',
    plugins: [
        // @note - Bundle analyzer
        // new require('webpack-bundle-analyzer').BundleAnalyzerPlugin(),
    ],
});
